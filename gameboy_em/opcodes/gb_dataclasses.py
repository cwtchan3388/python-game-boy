"""The dataclasses for Instructions and Operands"""
from dataclasses import dataclass
from typing import Literal, TypedDict


class OperandDict(TypedDict):
    """The Operand as a dict"""
    immediate: bool
    name: str
    increment: bool | None
    decrement: bool | None
    bytes: int | None
    value: int | None
    adjust: Literal["+", "-"] | None


@dataclass(frozen=True)
class Operand:
    """The Operand dataclass"""
    immediate: bool
    name: str
    bytes: int | None
    value: int | None
    adjust: Literal["+", "-"] | None

    @classmethod
    def create_from_dict(
        cls,
        immediate: bool,
        increment: bool | None = None,
        decrement: bool | None = None,
        name: str | None = None,
        bytes: int | None = None,
        value: int | None = None,
    ):
        match (increment, decrement):
            case None, None: adjust = None
            case True, _: adjust = "+"
            case _, True: adjust = "-"
        return cls(
            immediate=immediate,
            name=name,
            bytes=bytes,
            value=value,
            adjust=adjust)

    # def __repr__(self):
    #     return f"{self.name} {self.immediate}"


@dataclass
class Instruction:
    """The Instruction dataclass"""
    opcode: int
    immediate: bool
    operands: list[Operand]
    cycles: list[int]
    bytes: int
    mnemonic: str
    flags: dict
    comment: str = ""

    @classmethod
    def create_from_dict(
        cls,
        opcode: int,
        immediate: bool,
        operands: list[OperandDict],
        cycles: list[int],
        bytes: int,
        mnemonic: str,
        comment: str = "",
        flags: dict = None
    ):
        return cls(
            opcode=opcode,
            immediate=immediate,
            operands=[Operand.create_from_dict(**operand) for operand in operands],
            cycles=cycles,
            bytes=bytes,
            mnemonic=mnemonic,
            flags=flags,
            comment=comment)

    def __repr__(self):
        return (f"{self.opcode: #04x} {self.mnemonic: >4} "
                f"[{' '.join(str(operand) for operand in self.operands)}]"
                f"{'; ' + self.comment if self.comment else ''}")