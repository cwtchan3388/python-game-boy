"""opcode parser"""
import json
import os
from typing import List, Literal, TypedDict

import gameboy_em.opcodes.gb_dataclasses
from gameboy_em.opcodes.utils import base16


class OperandDict(TypedDict):
    """The Operand as a dict"""
    immediate: bool
    name: str
    bytes: int
    value: int | None
    adjust: Literal["+", "-"] | None


class InstructionDict(TypedDict):
    """The Instruction as a dict"""
    opcode: int
    immediate: bool
    operands: list[OperandDict]
    cycles: list[int]
    bytes: int
    mnemonic: str
    comment: str = ""


def get_instructions() -> list[gameboy_em.opcodes.gb_dataclasses.Instruction]:
    decoded_json = json.load(open(
        os.path.join(os.getenv("HOME"), "projects", "python-game-boy", "gameboy_em", "opcodes", "Opcodes.json")))
    return {
        key: {base16(opcode): gameboy_em.opcodes.gb_dataclasses.Instruction.create_from_dict(base16(opcode), **decoded_json[key][opcode])
              for opcode in decoded_json[key]}
        for key in decoded_json
    }



if __name__ == "__main__":
    print(get_instructions())
