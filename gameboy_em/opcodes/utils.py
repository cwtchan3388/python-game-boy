"""opcode utils"""
from functools import partial


base16 = partial(int, base=16)
